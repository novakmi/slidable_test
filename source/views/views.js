/**
	For simple applications, you might define all of your views in this file.  
	For more complex applications, you might choose to separate these kind definitions 
	into multiple files under this folder.
*/

enyo.kind({
       name: "SlideMenuItem",
       kind: "onyx.MenuItem",
       selected: false,
       select: function(val) {
          enyo.log("select called " + this.name + " val " + val);
          selected = val;
       },
});

enyo.kind({
       name: "SlideMenuItemInd",
       kind: "SlideMenuItem",
       components: [
	        {name: "sel", kind: "enyo.Control", classes: "slide-sel"},
       		{name: "label", kind: "enyo.Control"},
       ],
       create: function() {
       		this.inherited(arguments);
       	 	enyo.log("SlideMenuItemInd create");
       	 	this.$.label.content = this.content
       },
       select: function(val) {
       	  this.inherited(arguments);
       	  enyo.log("SlideMenuItemInd select called " + this.name + " val " + val);
       	  if (val) {
       	  	//this.$.sel.setContent("*");
       	  	this.$.sel.applyStyle("background", "#101010");
		  } else {
		     this.$.sel.applyStyle("background", null);
		     //this.$.sel.setContent("");
		  }
	   },
});



enyo.kind({
	name: "SlideMenu",
	kind: "enyo.Slideable",
 	handlers: {
        onSelect: 'itemSelected'
    },
	components: [
		{kind: "FittableRows", classes: "enyo-fit", components: [
				{name: "info", kind: "Scroller", classes: "enyo-fit", components: [
					{name: "group", kind: "enyo.Group", classes: "onyx-groupbox settings",/*, highlander: true, /*onchange: "mapTypeChange",*/ components: [
						{name: "items", kind: "onyx.GroupboxHeader", classes: "slide-groupbox", content: "Items1"},
						{name: "item1", kind: "SlideMenuItemInd", content: "Item 1", classes: "slide-item",/*mapType: "road", icon: "images/map-type-road.png",*/ active: true},
                        {name: "item2", kind: "SlideMenuItemInd", content: "Item 2", classes: "slide-item", /*mapType: "aerial", icon: "images/map-type-satellite.png"*/},
                        {name: "item3", kind: "SlideMenuItemInd", content: "Item 3", classes: "slide-item", /*mapType: "birdseye", icon: "images/map-type-bird-eye.png"*/},
                        {name: "item4", kind: "SlideMenuItemInd", content: "Item 4", classes: "slide-item", /*mapType: "birdseye", icon: "images/map-type-bird-eye.png"*/},
                        {name: "item5", kind: "SlideMenuItemInd", content: "Item 5", classes: "slide-item", /*mapType: "birdseye", icon: "images/map-type-bird-eye.png"*/},
                        {classes: "onyx-menu-divider", classes: "slide-separator"},
                        {name: "item6", kind: "SlideMenuItemInd", content: "Item 5", classes: "slide-item", /*mapType: "birdseye", icon: "images/map-type-bird-eye.png"*/},
					]}
				]},
		]},
	],
	min: -100,
	max: 0,
	value: -100,
	unit: "%",

	create: function() {
		this.inherited(arguments);
	 	enyo.log("create");
	},
	toggle: function() {
		this.toggleMinMax()
	},
	valueChanged: function() {
		enyo.log("valueChanged");
		this.inherited(arguments);
	},
    itemSelected: function (sender, event) {
            enyo.log('Menu Item Selected: ' + event.originator.content);
            // go through all items and select the event.originator
            var components = this.getComponents();
			enyo.log("found components ", components.length);
			for (var i = 0; i <  components.length; i++) {
				enyo.log(components[i].kind, " ", components[i].name);
				if (components[i].select) {
					enyo.log("Unselectinh ", components[i].name, " ", components[i].content,  " ", components[i].kind);
					components[i].select(false);
				} else {
					enyo.log("skipping, not slideMenuItem");
				}
			}
            event.originator.select(true)
            this.toggle();
    },
});



enyo.kind({
	name: "myapp.MainView",
	//kind: "FittableRows",
	classes: "app",
	//fit: true,
	components:[
		//{kind: "onyx.Toolbar", content: "Hello World"},
		{kind: "FittableRows", classes: "enyo-fit", components: [

            {kind: "FittableRows", classes: "enyo-fit", components: [
                {kind: "onyx.Toolbar", components: [
                       {kind: "onyx.Button", content: "Tap me", ontap: "helloWorldTap"}
                ]},
                { kind : enyo.Control, name: "label", content: "Selected:"},
                { kind : enyo.Control, name: "item", content: "N/A"},
//                {kind: "enyo.Scroller", fit: true, components: [
//                                {name: "main", classes: "nice-padding", allowHtml: true}
//                            ]},
            ]},
            {name: "menu", kind: "SlideMenu", classes:"slide-menu", onSelect: "myItemSelected"},
        ]},

	],
	helloWorldTap: function(inSender, inEvent) {
		this.$.menu.toggle();
		/*this.$.main.addContent("The button was tapped.<br/>");*/
	},

	create: function() {
		this.inherited(arguments)
        //this.$.label.setContent("Label: ")
	},
	myItemSelected: function (sender, event) {
		enyo.log('Menu Item Selected: ' + event.originator.content);
		this.$.item.setContent(event.originator.content)
	},

});
